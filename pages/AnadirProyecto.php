<?php
$id=$_GET['id'];

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GQLT</title>


    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">GoalQuality</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../pages/login.php">GoalQuality</a>
            </div>


            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                
                <!-- /.dropdown -->
          
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            
            </nav>



        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Añadir Proyecto</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" method="POST" enctype="multipart/form-data" action="../controller/proyecto/saveProyecto.php">
                                        <div class="form-group">
                                            <label>Nombre</label>
                                            <input name="pry_Nombre" id="pry_Nombre" class="form-control" placeholder="Nombre">
                                        </div>
                                        <div class="form-group">
                                            <label>Descripcion</label>
                                            <input name="pry_Descripcion" id="pry_Descripcion" class="form-control" placeholder="Descripcion">
                                        </div>
                                        <div class="form-group">
                                            <label>Usuario</label>
                                            <input name="us_Id" id="us_Id" class="form-control" value="<?php echo $id;?>">
                                        </div>
                                        
                                        <button type="submit" class="btn btn-default">Añadir</button>
                                    </form><br>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <?php 
							include_once '../conexion/conexion.php';
							$c = new Conexion();
							$conn = $c->getConexion();
							$sql = "SELECT * FROM proyecto WHERE pry_Estado=0 "; 
							$result =$conn->query($sql);
							
							?> 
							
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Nombre del proyecto</th>
                                        <th>Descripcion</th>
                                        <th>Eliminar</th>
                                        <th>Editar</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php while ($row = mysqli_fetch_row($result)){  ?> 
                                    <tr class="odd gradeX">
                                        <td><a href="../pages/standar.php?id=<?php echo $row[0]; ?>"><?php echo $row[1]; ?></a></td>
                                        <td><?php echo $row[2]; ?></td>
										<td><form role="form" method="POST" enctype="multipart/form-data" action="../controller/proyecto/deleteProyecto.php">
											<input type="hidden" name="pry_Id" id="pry_Id" value="<?php echo $row[0]; ?>" class="form-control" placeholder="ID">
                                            <input type="hidden" name="id" value="<?php echo $id;?>">
											<button type="submit" class="btn btn-default">Eliminar</button>
											</form>
										</td>
										<td><div class="titulo_boton">
  <a style='cursor: pointer;' onClick="muestra_oculta('<?php echo  $row[0];?>')" title="" class="boton_mostrar">Editar</a>
</div>

<div style="display: none" id="<?php echo  $row[0];?>">
											<form role="form" method="POST" enctype="multipart/form-data" action="../controller/proyecto/editProyecto.php">
											<input type="hidden" name="pry_Id" id="pry_Id" value="<?php echo $row[0]; ?>" class="form-control" placeholder="ID">
											Nombre
											<input name="pry_Nombre" id="pry_Nombre" value="<?php echo $row[1]; ?>" class="form-control" placeholder="Nombre">
											Descripcion
											<input name="pry_Descripcion" id="pry_Descripcion" value="<?php echo $row[2]; ?>" class="form-control" placeholder="Acrónimo">
											<button type="submit" class="btn btn-default">Editar</button>
											</form>
</div></td>
                                    </tr>                                   
                                  <?php }  ?>   
                                </tbody>
                            </table>
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
	
	
function muestra_oculta(id){
if (document.getElementById){ //se obtiene el id
var el = document.getElementById(id); //se define la variable "el" igual a nuestro div
el.style.display = (el.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div
}
}
window.onload = function(){/*hace que se cargue la función lo que predetermina que div estará oculto hasta llamar a la función nuevamente*/
muestra_oculta('contenido');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */
}
    </script>

</body>

</html>
