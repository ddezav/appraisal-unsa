
<?php

require_once("../conexion/conexion.php");
$c = new Conexion();
$conn = $c->getConexion();

$idCriterio = $_GET['id'];
$consulta =  "SELECT * FROM `evidencia` WHERE `cr_Id` =".$idCriterio;
$result =$conn->query($consulta);

$outpe = array();
$outpe = $result->fetch_all(MYSQLI_ASSOC);
$prueba="";
$idEvidencia = "";
$evidenciaNombre = "";
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Evidencias de las tareas</title>
  <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
  <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">
  <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
  <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="../vendor/metisMenu/metisMenu.min.js"></script>
  <script src="../dist/js/sb-admin-2.js"></script>

  <script src="../vendor/datatables/js/jquery.dataTables.min.js"></script>
  <script src="../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
  <script src="../vendor/datatables-responsive/dataTables.responsive.js"></script>



  <script type="text/javascript">


    function mostrar(id){
        //window.locatio.replace("../pages/editarEvidencia.php?id="+id);

        $.ajax({
          url: "../controller/evidencia/getEvidenciaById.php",
          type: 'POST',
          data: {"evidencia_Id":id},      
          success: function(result){
            if(result){
              var objs = $.parseJSON(result);
              $('#e_evidencia_nombre').val(objs[0]['evi_Nombre']);
              $('#e_evidencia_descripcion').val(objs[0]['evi_Descripcion']);
              $("#e_evidencia_estado option[value="+objs[0]['evi_Estado']+"]").attr('selected', 'selected');
              $('#e_evidencia_url').val(objs[0]['evi_urlws']);
              $('#e_evidencia_file').attr('value',objs[0]['evi_Archivo']);            
              $('#e_idEvi').val(id);          
                //window.location.replace("../index");
                $('#modalEditForm').modal('show');
              }
              else{ 
                alert('ocurrio algun ERROR, vuelva a intentarlo ');
              }   
            }
          });

      }
      function update(id){
    //rellenar los grupos 
    $.ajax({
      type: 'GET',
      url: '../controller/evidencia/evidenciaEliminar.php',
      data: {
        idMetrica: id,
      },
      success: function (data) {
        console.log("codigo="+data);
        location.reload();
        alert("se elimino correctamente!");
      }
    });                        
  }
</script>
<script>
  $("#modalForm").on('shown.bs.modal', function(){
    $.ajax({
      url: "../controller/evidencia/getEvaluacion.php",
      type: 'POST',
      data: {"data":"fsdf"},      
      success: function(result){
        if(result){
          var i=0;
          var objs = $.parseJSON(result); 

          for(var i=0; i<objs.length;i++){

           $('#FK_evaluacion_id').append($("<option></option>").attr("value",objs[i]['eva_Id']).text(objs[i]['eva_Nombre'])); 
         }

       }
       else{
        alert('ocurrio algun ERROR, vuelva a intentarlo ');
      }   
    },
    error: function(){
      alert('Ocurrio un erro en el Proceso');
    }
  });
  });

  function editEvidencia(id){
    var file_data = $('#e_evidencia_file').prop('files')[0];   
    var form_data = new FormData();  
    var idCriterio = id;
    form_data.append('evidencia_nombre', $("#e_evidencia_nombre").val());
    form_data.append('evidencia_descripcion', $("#e_evidencia_descripcion").val());
    form_data.append('evidencia_estado', $("#e_evidencia_estado").val());
    form_data.append('evidencia_url', $("#e_evidencia_url").val());   
    form_data.append('evidencia_file', file_data);
    form_data.append('id', $("#e_idEvi").val());

    $.ajax({
      url: '../controller/evidencia/updateEvidencia.php',       
      type: 'POST',  
      enctype: 'multipart/form-data',
      processData: false,
      contentType: false,
      data: form_data,      
      success: function(result){
        if(result){
          alert("cambiado correctamente!");              
          window.location.replace("../pages/tarea.php?id="+idCriterio+"");
        }
        else{
          alert('ocurrio algun ERROR, vuelva a intentarlo ');
        }   

      },
      error: function(){
        alert('Ocurrio un erro en el Proceso');
      }
    });

  }

  function addEvidencia(id){

    var idCriterio = id;
    var file_data = $('#evidencia_file').prop('files')[0];   
    var form_data = new FormData();  

    form_data.append('evidencia_nombre', $("#evidencia_nombre").val());
    form_data.append('evidencia_descripcion', $("#evidencia_descripcion").val());
    form_data.append('evidencia_estado', $("#evidencia_estado").val());
    form_data.append('evidencia_url', $("#evidencia_url").val());
    form_data.append('FK_evaluacion_id', idCriterio);
    form_data.append('evidencia_file', file_data);
    $.ajax({
      url: '../controller/evidencia/saveEvidencia.php',       
      type: 'POST',  
      enctype: 'multipart/form-data',
      processData: false,
      contentType: false,
      data: form_data,     
      success: function(result){
        if(result){
          alert("agregado correctamente!");              
          window.location.replace("../pages/tarea.php?id="+idCriterio+"");
        }
        else{
          alert('ocurrio algun ERROR, vuelva a intentarlo ');
        }   

      },
      error: function(){
        alert('Ocurrio un erro en el Proceso');
      }
    });
  }
</script>
</head>


<body>

  <div id="wrapper">
    <?php
    include '../pages/menu.php';
    ?>
    <div id="page-wrapper">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Gestion de Evidencias</h1>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <div class="row">

        <div class="col-lg-6">
          <div class="panel panel-success">
            <div class="panel-heading">
              AGREGAR UNA NUEVA EVIDENCIA
            </div>
            <div class="panel-body">
              <div class="col-lg-3">
                <label class="control-label col-sm-5">Agregar:</label>
              </div>
              <div class="col-lg-9">
                 <button class="btn btn-success btn-lg" data-toggle="modal" data-target="#modalForm">
                Agregar Evidencia
              </button>
              </div>
            </div>
            <div class="panel-footer">
              
            </div>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="panel panel-danger">
            <div class="panel-heading">
              GENERAR REPORTE DE EVIDENCIAS
            </div>
            <div class="panel-body">
              <div class="col-lg-3">
                <label class="control-label col-sm-5" >Generar Reporte:</label>
              </div>
              <div class="col-lg-9">
                 <button class="btn btn-danger btn-lg" data-toggle="modal" data-target="#modalForm" >
                Reporte de Evidencias
              </button>
              </div>
            </div>
            <div class="panel-footer">
              
            </div>
          </div>
        </div>
        <div class="col-lg-12"> 
          <div class="panel panel-default">
            <div class="panel-heading">
              Evidencias
            </div>

            <!-- /.panel-heading -->
            <script type="text/javascript">
              $(document).ready(function(){
                $('#reemplazosT').DataTable({
                 "language":{
                  "lengthMenu": "Se muestran _MENU_ registros por pagina",
                  "info": "Mostrando pagina _PAGE_ de _PAGES_",
                  "infoEmpty": "No hay registros disponibles",
                  "infoFiltered": "(filtrada de _MAX_ registros)",
                  "loadingRecords": "Cargando...",
                  "processing":     "Procesando...",
                  "search": "Buscar:  ",
                  "zeroRecords":    "No se encontraron registros coincidentes",
                  "paginate": {
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                  },          
                }
              }); 
              }); 
            </script>
            <div class="panel-body">  
              <div class="table-responsive">
                <table id='reemplazosT' class='table table-hover'>
                  <thead>
                    <tr >
                      <th>ID</th>
                      <th>Nombre Metrica</th>
                      <th>Web Service</th>
                      <th>Documento</th>
                      <th>Operaciones</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php
                    $obj = $outpe;
                    for ($i = 0; $i < count($obj); $i++) {
                      $id = $obj[$i]['evi_Id'];
                      $nombre = $obj[$i]['evi_Nombre'];
                      $ws = $obj[$i]['evi_Archivo'];
                      $archivo = $obj[$i]['evi_urlws'];

                      if (strcmp($obj[$i]['evi_Estado'],"2") == 0) {
                        echo '
                        <tr class="success">.
                        <td>'.$id.'</td>
                        <td>'.$nombre .'</td>
                        <td>'.$ws .'</td>
                        <td >'.$archivo .'</td>

                        <td><button type="button" class="btn btn-warning" onclick= "mostrar('.$id.');">Editar</button> <button type="button" class="btn btn-danger" onclick= "update('.$id.');">Eliminar</button> <a href="../pages/metricasVista.php?idMetrica='.$id.'" class="btn btn-primary" role="button" aria-pressed="true">Ver Metrica</a> </td>';
                      }

                      else if (strcmp($obj[$i]['evi_Estado'],"1") == 0) {
                        echo '
                        <tr class="warning">.
                        <td>'.$id.'</td>
                        <td>'.$nombre .'</td>
                        <td>'.$ws .'</td>
                        <td >'.$Archivo .'</td>
                        <td><button type="button" class="btn btn-warning" onclick= "mostrar('.$id.');">Editar</button> <button type="button" class="btn btn-danger" onclick= "update('.$id.');">Eliminar</button> <a href="../pages/metricasVista.php?idMetrica='.$id.'" class="btn btn-primary" role="button" aria-pressed="true">Ver Metrica</a></td>';

                      }
                      else {  
                        echo '
                        <tr class="danger">.
                        <td>'.$id.'</td>
                        <td>'.$nombre .'</td>
                        <td>'.$ws .'</td>
                        <td >'.$archivo .'</td>
                        <td><button type="button" class="btn btn-warning" onclick= "mostrar('.$id.');">Editar</button> <button type="button" class="btn btn-danger" onclick= "update('.$id.');">Eliminar</button> <a href="../pages/metricasVista.php?idMetrica='.$id.'" class="btn btn-primary" role="button" aria-pressed="true">Ver Metrica</a></td>';
                      }
                    }
                    ?>

                  </tbody>
                </table>
                <hr>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="modal_exito" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Esta seguro que desea eliminar esta evidencia.</h3>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" onclick="mostrarMensaje(5)"  data-dismiss="modal">Eliminar</button>
          <button type="button" class="btn btn-default"  data-dismiss="modal">Cancelar</button>
        </div>
      </div>

    </div>
  </div>

  <div class="modal  fade" id="modalForm" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
          </button>
          <h4 class="modal-title" id="myModalLabel" style="text-align:center;font-weight: bold; ">Registrar Evidencia</h4>
        </div>
        <div class="modal-body">
          <p class="statusMsg"></p>
          <form role="form" method="POST" enctype="multipart/form-data" action="../controller/evidencia/saveEvidencia.php">

            <div class="form-group">
              <label>Nombre</label>
              <input class="form-control" name="evidencia_nombre" id="evidencia_nombre">
              <p class="help-block">Ingrese un nombre para la evidencia.</p>
            </div>
            <div class="form-group">
              <label>Descripción</label>
              <textarea class="form-control" rows="3" id="evidencia_descripcion" name="evidencia_descripcion"></textarea>
            </div>
            <div class="form-group">
              <label>Estado</label>
              <select class="form-control" id="evidencia_estado" name="evidencia_estado">
                <option value="2">Bueno</option>
                <option value="1">Regular</option> 
                <option value="0">Malo</option>     
              </select>
            </div>
            <div class="form-group">
              <label>Archivo </label>
              <input id="evidencia_file" name="evidencia_file" type="file">
            </div>
            <div class="form-group">
              <label>URL</label>
              <textarea id="evidencia_url" name="evidencia_url" class="form-control" rows="3"></textarea>
            </div>
          </form>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <?php echo '<button type="button" class="btn btn-primary submitBtn" onclick="addEvidencia('.$idCriterio.')">Agregar</button>' ?>
        </div>
      </div>
    </div>
  </div>



  <div class="modal  fade" id="modalEditForm" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">
            <span aria-hidden="true">×</span>
            <span class="sr-only">Close</span>
          </button>
          <h4 class="modal-title" id="myModalLabel" style="text-align:center;font-weight: bold; ">Editar Evidencia</h4>
        </div>

        <!-- Modal Body -->
        <div class="modal-body">
          <p class="statusMsg"></p>
          <form role="form" method="POST" enctype="multipart/form-data" action="../controller/evidencia/saveEvidencia.php">


            <input type="hidden" id="e_idEvi" name="e_idEvi">
            <div class="form-group">
              <label>Nombre</label>
              <input class="form-control" name="e_evidencia_nombre" id="e_evidencia_nombre">
              <p class="help-block">Ingrese un nombre para la evidencia.</p>
            </div>
            <div class="form-group">
              <label>Descripción</label>
              <textarea class="form-control" rows="3" id="e_evidencia_descripcion" name="e_evidencia_descripcion"></textarea>
            </div>
            <div class="form-group">
              <label>Estado</label>
              <select class="form-control" id="e_evidencia_estado" name="e_evidencia_estado">
                <option value="2">Bueno</option>
                <option value="1">Regular</option> 
                <option value="0">Malo</option>     
              </select>
            </div>

            <div class="form-group">
              <label>Archivo </label>
              <input id="e_evidencia_file" name="e_evidencia_file" type="file">
            </div>

            <div class="form-group">
              <label>URL</label>
              <textarea id="e_evidencia_url" name="e_evidencia_url" class="form-control" rows="3"></textarea>
            </div>
          </form>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <?php echo '<button type="button" class="btn btn-primary submitBtn" onclick="editEvidencia('.$idCriterio.')">Agregar</button>' ?>
        </div>
      </div>
    </div>
  </div>
</body>

</html>


