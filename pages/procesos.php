
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GQLT</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <?php
        include '../pages/menu.php';
        ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Seguimiento de Evidencias</h1>
                </div>
                
            </div>
               <div class="panel panel-default">
                    <div class="panel-heading">
                        TABLERO DE CONTROL 
                    </div>
                    <div class="panel-body">
                        <p> Los tableros de control o dashboards permiten medir el estado actual de una serie de indicadores y evaluarlos frente a unos objetivos. De esta forma, facilitan la toma de decisiones y aumentan su precisión, minimizando la probabilidad de error.</p>
                    </div>
                    
                </div>
            <div class="col-lg-4">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        Estado Bueno 
                    </div>
                    <div class="panel-body">
                        <p>Informa una correcta evaluacion de la evidencia de acuerdo a una tareas de una actividad por cada proceso</p>
                    </div>
                    <div class="panel-footer">
                        Panel Bueno
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        Estado Regular 
                    </div>
                    <div class="panel-body">
                        <p>Detenerse por precaución de la evidencia o evaluacion por culminar de acuerdo a una tareas de una actividad por cada proceso</p>
                    </div>
                    <div class="panel-footer">
                        Panel Regular
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        Estado Malo 
                    </div>
                    <div class="panel-body">
                        <p>Evaluacion fallida de la evidencia de acuerdo a una tareas de una actividad por cada proceso</p>
                    </div>
                    <div class="panel-footer">
                        Panel Malo
                    </div>
                </div>
            </div>

<div class="col-lg-12">
              <div class="panel panel-default">
                    <div class="panel-heading">
                        SISTEMA DE GESTION DE EVIDENCIAS - CRID 
                    </div>
                    <div class="panel-body">
                        <p> Crear evidencias para ser evaluadad por cada tarea.</p>
                        <strong><p> Agregar Evidencia</p>
                        <p> Modificar Evidencia</p>
                        <p> Eliminar Evidencia</p></strong>
                    </div>
                    
                </div>
            </div>
        </div>

        <!-- /#page-wrapper -->

    </div>

    <!-- Menu -->

    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../vendor/raphael/raphael.min.js"></script>
    

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>