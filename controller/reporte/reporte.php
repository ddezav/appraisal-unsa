<?php
include_once '../../conexion/conexion.php';
header('Content-Type: text/html; charset=ISO-8859-1');
$c = new Conexion();
$conn = $c->getConexion();
$idv=$_GET['idm'];


$sql = "SELECT est_nombre FROM `estandar` WHERE `est_Id`=".$idv;

$result1 =$conn->query($sql);
$row = $result1->fetch_assoc();
$nombreNorma = $row["est_nombre"];


function llenarModal($id){
	
			$c = new Conexion();
			$conn = $c->getConexion();
			
			$sql = "SELECT * FROM `criterio` WHERE `cr_Id`=".$id;

			$result1 =$conn->query($sql);
			$row = $result1->fetch_assoc();
			
	
				echo"
			<div class='modal' id=\"c$id\">
                <div class=modal-dialog>
                    <div class=modal-content>
                        <div class=modal-header>
                            <p class=modal-title>Descripci&oacuten del Criterio y Sus Evidencias</p>
                            <button type=button class=close data-dismiss=modal>&times;</button>
                        </div>
                        <div class=modal-body>
                            <div class=container-fluid>
                                <div class=row >
                                    <div class=col-md-6>
                                        <label><strong>Descripci&oacuten del Criterio</strong></label>
                                    </div>
                                    <div class=col-md-6>
                                        <p>".$row['cr_Descripcion']."</p>
                                    </div>
                                </div>
                                <div class=row >
                                    <div class=col-md-12>
                                        <label><strong>Evidencias</strong></label>
                                    </div>";
									$sqlEvidencias = "SELECT * FROM `evidencia` WHERE `cr_Id`=".$id;
									$result =$conn->query($sqlEvidencias);
									
									
									while($row = $result->fetch_assoc()) {
											echo "<div class=col-md-6>
                                        <p>".$row['evi_Nombre']."</p>
                                    </div>";
										echo "<div class=col-md-6>
                                        <p>".$row['evi_Descripcion']."</p>
                                    </div>";
									}
									
                                    
									
			echo "
                                </div>
                            </div>
                        </div>
                                            
                    </div>
                </div>
              </div>";
}


function dibujarCuadrado($valor,$id){
	//2-> verde 1-> amarillo 0->rojo
	switch ($valor) {
		case -1:
			//color plomo claro-no util
			$color="(96,96,96)";
			break;
	    
	    case 0:
	       //color rojo-no implemented 
	    	$color="(255,0,0)";
	        break;
	    
	    case 1:
	    	//color amarillo - largamente implementado
	    	$color="(255,255,51)";
	    	break;
	    case 2:
	    	//color verde -fully implemented
	    	$color="(0,153,0)";
	    	break;
	    default:
	        $color="(0,0,0)";
	}	
	//<svg width='18' height='18'>
    //          <rect width='300' height='100' style='fill:rgb".$color.";stroke-width:3;stroke:rgb(0,0,0)' />
     //       </svg>
    if($id==-1){
        return  "
            <svg width='15' height='15'>
              <rect width='300' height='100' style='fill:rgb".$color.";stroke-width:3;stroke:rgb(0,0,0)' />
            </svg>";
    }else{
        return  "<a data-toggle='modal' href=\"#c".$id."\"><svg width='15' height='15'>
              <rect width='300' height='100' style='fill:rgb".$color.";stroke-width:3;stroke:rgb(0,0,0)' />
            </svg></a>";
    }
}

echo "
<!DOCTYPE html>
<head>

    

    <style type='text/css'>

        table{
          table-layout: fixed;
          width: 200px;
			border-top:0px solid #ddd;
        }
		td{
			border-top-color: white;
		}
        #tabla1{
            width: 200px;
            position: absolute;
            background: white;
        }
        
        #tabla2{
            margin-left: 200px;
            width:64%;
        }
        .registrarAlumnos{
            margin-top: 40px;
        }
    </style>


    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <meta name='description' content=''>
    <meta name='author' content=''>
    <meta http-equiv='Content-Type' content=text/html; charset=utf-8> 

    <title>Calidad</title>


    <!-- Bootstrap Core CSS -->
    <link href='../../vendor/bootstrap/css/bootstrap.min.css' rel='stylesheet'>

    <script src='../../vendor/jquery/jquery.min.js'></script>
    <script src='../../vendor/bootstrap/js/bootstrap.min.js'></script>

    <!-- MetisMenu CSS -->
    <link href='../../vendor/metisMenu/metisMenu.min.css' rel='stylesheet'>

    <!-- DataTables CSS -->
    <link href='../../vendor/datatables-plugins/dataTables.bootstrap.css' rel='stylesheet'>

    <!-- DataTables Responsive CSS -->
    <link href='../../vendor/datatables-responsive/dataTables.responsive.css' rel='stylesheet'>

    <!-- Custom CSS -->
    <link href='../../dist/css/sb-admin-2.css' rel='stylesheet'>
    <!-- Custom Fonts -->
    <link href='../../vendor/font-awesome/css/font-awesome.min.css' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js'></script>
        <script src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js'></script>
    <![endif]-->

</head>

<body>

    <div id='wrapper'>

        <!-- Navigation -->
        <nav class='navbar navbar-default navbar-static-top' role='navigation' style='margin-bottom: 0'>
            <div class='navbar-header'>
                <a class='navbar-brand' href='../../'>Goal Quality</a>
            </div>
            <!-- /.navbar-header -->


            <div class='navbar-default sidebar' role='navigation'>
                <div class='sidebar-nav navbar-collapse'>
                   
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
<div id='page-wrapper'>
            <div class='row'>
                <div class='col-lg-12'>
                    <h1 class='page-header'>Seguimiento - ".$nombreNorma."</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class='row'>
                <div class='col-lg-12'>
                    <div class='panel panel-default'>
                        
                        <div class='panel-body'>
                            <div class='row'>
                                <div class='col-lg-6'>
                                <a href = '../../reportepdf/'>Generar Reporte </a>  <br>                    
                                <a href = '../../reportepdf/tavance.html'>Ir a Avance por Porcentaje</a>                      
										
	";
	
$idv=$_GET['idm'];


$sql = "SELECT *  FROM `criterio` WHERE `cr_Idh` IS NULL and `est_Id`=".$idv;

$result1 =$conn->query($sql);

$tablasColumna=array(array());
$tablaActividades = array();

if ($result1->num_rows > 0) {
	
    // output data of each row
    while($row = $result1->fetch_assoc()) {
    	$idPrimerNivel=$row["cr_Id"];

        $sql = "SELECT *  FROM `criterio` WHERE `cr_Idh`=".$idPrimerNivel." and `est_Id`=".$idv;
		$result2 =$conn->query($sql);
        array_push($tablaActividades, $result2->num_rows);
		$temp=array();



		while($row = $result2->fetch_assoc()) {
		
			$idSegundoNivel=$row["cr_Id"];
			$sql = "SELECT *  FROM `criterio` WHERE `cr_Idh`=".$idSegundoNivel." and `est_Id`=".$idv;
			$result3 =$conn->query($sql);
            array_push($temp,$result3->num_rows);    
            			
			while($row = $result3->fetch_assoc()) {
		
				$idTercerNivel = $row["cr_Id"];
		
			}
		}

		array_push($tablasColumna, $temp);
		
    }
    
    array_shift($tablasColumna);

    $maximaColumna =0; 
    for ($i=0; $i <count($tablasColumna) ; $i++) { 

        if( $maximaColumna < count($tablasColumna[$i]) ){
            $maximaColumna=count($tablasColumna[$i]);
        }
    }

    for ($i=0; $i <count($tablasColumna) ; $i++) { 

        while( count($tablasColumna[$i])<$maximaColumna ){
            array_push($tablasColumna[$i], 0);
            
        }
    }


    $maximos = array_fill(0, count($tablasColumna[0]), 0);

    for ($i=0; $i <count($tablasColumna[0]) ; $i++) { 

		for ($j=0; $j <count($tablasColumna) ; $j++) { 
			//echo $tablasColumna[$j][$i];

			if($maximos[$i]<$tablasColumna[$j][$i]){
				$maximos[$i]=$tablasColumna[$j][$i];
			}
		}
	}
$maximoActividades = max($tablaActividades);

//COMIENZO PARA DIBUJAR LA TABLA

$sql = "SELECT *  FROM `criterio` WHERE `cr_Idh` IS NULL and `est_Id`=".$idv;
$result1 =$conn->query($sql);
$procesos = $result1->num_rows;

	
    // output data of each row
    while($row = $result1->fetch_assoc()) {
    	echo "<table class='table table-sm' id=tabla".$row["cr_Id"].">";
        //echo "id: " . $row["cr_Id"]. " - Name: " . $row["cr_Indice"]. " " . $row["cr_Idh"]. "<br>";
        $idPrimerNivel=$row["cr_Id"];

        $sql = "SELECT *  FROM `criterio` WHERE `cr_Idh`=".$idPrimerNivel." and `est_Id`=".$idv;
		$result2 =$conn->query($sql);
		$temp=array();		
		
        $diferenciaActividades= $maximoActividades-($result2->num_rows);
        //echo $diferenciaActividades;
        $indiceMaximo=0;

		while($row = $result2->fetch_assoc()) {
			//echo "id: " . $row["cr_Id"]. " - Name: " . $row["cr_Indice"]. " " . $row["cr_Idh"]. "<br>";
            //LLENAR ACTVIDADADES
			echo "<tr><td>".dibujarCuadrado($row["cr_estado"],$row["cr_Id"])."</td></tr>";
			echo llenarModal($row["cr_Id"]);

			$idSegundoNivel=$row["cr_Id"];
			$sql = "SELECT *  FROM `criterio` WHERE `cr_Idh`=".$idSegundoNivel." and `est_Id`=".$idv;
			$result3 =$conn->query($sql);
			$filasMaximo=$maximos[$indiceMaximo];
			$diferencia=$filasMaximo-($result3->num_rows);
			
			while($row = $result3->fetch_assoc()) {
                //LLENAR TAREAS
				echo "<tr class='collapse nivel".$indiceMaximo."'><td>".dibujarCuadrado($row["cr_estado"],$row["cr_Id"])."</td></tr>";
				echo llenarModal($row["cr_Id"]);
				$idTercerNivel = $row["cr_Id"];
			}
			for ($k=0; $k <$diferencia ; $k++) { 
                //LLENAR TAREAS FALTANTES
				echo "<tr class='collapse nivel".$indiceMaximo."'><td>".dibujarCuadrado(-1,-1)."</td></tr>";
			}

			//$indiceMaximo=($indiceMaximo+1)%count($maximos);
            $indiceMaximo=($indiceMaximo+1);
            

		}
        //LLENAR ACTIVIDADES FALTANTES
        for ($k=0; $k <$diferenciaActividades ; $k++) { 
            echo "<tr><td>".dibujarCuadrado(-1,-1)."</td></tr>";
        }

		echo "</table>";
    }
	
	//LLENAR TABLA TAREAS Y ACTIVIDADES
    echo "<table class='table table-sm' style='margin-left: 100px;' id='cabecera'>";
    $act ="ACT";
    $tarea = "TAREA";
    for ($i=0; $i <$maximoActividades ; $i++) { 
    	echo "<tr><th><div data-toggle='collapse' data-target='.nivel".$i."'>".$act." ".($i+1)."</div></th></tr>";
    	for ($j=0; $j <($maximos[$i]) ; $j++) { 
    		echo "<tr class='collapse nivel".$i."'><td>".$tarea." ".($j+1)."</td></tr>";
    	}
    }
    echo "</table>";

	$sql = "SELECT *  FROM `criterio` WHERE `cr_Idh` IS NULL and `est_Id`=".$idv;

	$result1 =$conn->query($sql);
	echo "<table class='table table-sm' id=abajo
	 style='margin-left: 200px; width:".($procesos*50)."px;'> ";
	echo "<tr>";
	while($row = $result1->fetch_assoc()) {
    	
        //echo "id: " . $row["cr_Id"]. " - Name: " . $row["cr_Indice"]. " " . $row["cr_Idh"]. "<br>";
        $idPrimerNivel=$row["cr_Id"];

        echo "<td>".$row["cr_Nombre"]."</td>";
	}
	echo "</tr>";
	echo "</table>";
	
	
	echo "<table class='table table-sm table-borderless' style='margin-left: 100px;'>
    <tr><td>No Utilizado</td>
	<td>No Completado</td>
	<td>Completado a la Mitad</td>
	<td>Completado Totalmente</td>
	</tr>
	<tr><td>".dibujarCuadrado(-1,-1)."</td>
    <td>".dibujarCuadrado(0,-1)."</td>
	<td>".dibujarCuadrado(1,-1)."</td>
	<td>".dibujarCuadrado(2,-1)."</td>
	</tr>
	</table>
    ";
	
} else {
    echo "0 results";
}

echo "<script>
		$(document).ready(function () {
        	var p = ".$procesos.";
        	mrg=200;
        	for(i=1;i<=p;i++){
        		t=$('#tabla'+i);
        		t.css('margin-left',mrg+'px');
        		t.css('position','absolute');
        		mrg+=50;
        	}
        	   
        });

</script>";

echo "</div>
                                <!-- /.col-lg-6 (nested) -->
                            <div class='col-lg-12'>
                    
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>";

$conn->close();