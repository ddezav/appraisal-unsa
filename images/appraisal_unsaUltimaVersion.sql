-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-12-2018 a las 00:38:24
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0
create database APPRAISAL_UNSA;
use APPRAISAL_UNSA;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";



--
-- Base de datos: `appraisal_unsa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `criterio`
--

CREATE TABLE `criterio` (
  `cr_Id` int(11) NOT NULL,
  `cr_Indice` varchar(10) NOT NULL,
  `cr_Nombre` varchar(52) NOT NULL,
  `cr_Descripcion` varchar(200) NOT NULL,
  `cr_Idh` int(11) DEFAULT NULL,
  `cr_esHoja` int(1) DEFAULT NULL,
  `est_Id` int(1) DEFAULT NULL,
  `cr_estado` int(1) NOT NULL DEFAULT '0' COMMENT '"0" activo\n"1" inactivo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `criterio`
--

INSERT INTO `criterio` (`cr_Id`, `cr_Indice`, `cr_Nombre`, `cr_Descripcion`, `cr_Idh`, `cr_esHoja`, `est_Id`, `cr_estado`) VALUES
(1, '5.1', 'P1', '', NULL, 0, 1, 0),
(2, '5.2', 'P2', '', NULL, 0, 1, 1),
(3, '5.3', 'P3', '', NULL, 0, 1, 1),
(4, '5.1.1.1', 'TAREA1', '', 9, 1, 1, 1),
(5, '5.1.1.2', 'TAREA2', '', 9, 1, 1, 1),
(6, '5.2.1.1', 'TAREA3', '', 12, 1, 1, 1),
(7, '5.2.1.2', 'TAREA1', '', 12, 1, 1, 1),
(8, '5.2.1.3', 'TAREA2', '', 12, 1, 1, 1),
(9, '5.1.1', 'ACT1', '', 1, 0, 1, 0),
(10, '5.1.2', 'ACT2', '', 1, 0, 1, 0),
(11, '5.1.3', 'ACT3', '', 1, 0, 1, 0),
(12, '5.2.1', 'ACT1', '', 2, 0, 1, 0),
(13, '5.2.2', 'ACT2', '', 2, 0, 1, 0),
(14, '5,2,3', 'ACT3', '', 2, 0, 1, 0),
(15, '5.3.1', 'ACT1', '', 3, 0, 1, 0),
(16, '5.3.2', 'ACT2', '', 3, 0, 1, 0),
(17, '5.3.3', 'ACT3', '', 3, 0, 1, 0),
(18, '5.1', 'Adquisicion', 'Proceso de Adquisición', NULL, 0, 2, 0),
(19, 'INICIO', 'NTP 5.1.1', 'Fase inicial del proceso de adqusiicion de la NTP', 18, NULL, 2, 0),
(20, 'Propuestas', '5.1.2 PS', 'Preparación de Propuestas', 18, NULL, 2, 0),
(21, 'Contrato', '5.1.3 Contrato', 'Preparación y actualización del contrato', 18, NULL, 2, 0),
(22, 'SPV', 'NTP 5.1.4 Seguimiento Proveedor', '', 18, 0, 2, 0),
(23, 'ACFIN', 'NTP 5.1.5 Aceptación y Finalizacion', '', 18, 0, 2, 0),
(24, '5.1.1.1 ', 'Necesidades', '', 20, 1, 2, 0),
(25, '5.1.1.2', 'Requisitos', '', 20, 1, 2, 0),
(26, ' NECESIDAD', '5.1.1.1 Concepto de Necesidad', '', 20, 1, 2, 0),
(27, 'REQUISITOS', '5.1.1.2 Difinir requisitos del sistema', '', 20, 1, 2, 0),
(28, 'OBTREQ', '5.1.1.5 Obtención de requerimientos', '', 18, 1, 2, 0),
(29, 'CRITERIOS', '5.1.1.4 Medicion', '', 18, 1, 2, 0),
(30, 'REQADQ', '5.1.2.1 Requisitos de Adquisicion', '', 20, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detcab`
--

CREATE TABLE `detcab` (
  `dc_Id` int(11) NOT NULL,
  `cp_Id` int(11) DEFAULT NULL,
  `est_Id` int(11) DEFAULT NULL,
  `dc_Estado` int(1) DEFAULT '0' COMMENT '"0" activo\n"1" inactivo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estandar`
--

CREATE TABLE `estandar` (
  `est_Id` int(11) NOT NULL,
  `est_nombre` varchar(52) NOT NULL,
  `est_Acronimo` varchar(10) NOT NULL,
  `est_estado` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estandar`
--

INSERT INTO `estandar` (`est_Id`, `est_nombre`, `est_Acronimo`, `est_estado`) VALUES
(1, 'cmmi', 'cmmi', 1),
(2, 'NTP', 'NTP-ISO/IE', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evidencia`
--

CREATE TABLE `evidencia` (
  `evi_Id` int(11) NOT NULL,
  `evi_Nombre` varchar(52) NOT NULL,
  `evi_Descripcion` varchar(52) NOT NULL,
  `evi_Estado` int(1) NOT NULL COMMENT '"0" incompleto "1" en desarrollo "2" completo',
  `cr_Id` int(11) NOT NULL,
  `evi_Archivo` varchar(52) NOT NULL,
  `evi_urlws` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `evidencia`
--

INSERT INTO `evidencia` (`evi_Id`, `evi_Nombre`, `evi_Descripcion`, `evi_Estado`, `cr_Id`, `evi_Archivo`, `evi_urlws`) VALUES
(1, 'evidencia34', 'gdfgfdg', 1, 1, 'calidad.sql', 'gdfgfdgdfg'),
(2, 'Personal Capacitado', 'Caracteristicas que el personal contratado esta deb', 2, 1, 'EVIDENCIAS.xlsx', 'https://docs.google.com/spreadsheets/d/1ZMpg_hhjbvKvkXDbayX3d2mNNZLZ9y5eAo_QevMgQbQ/edit#gid=0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proycab`
--

CREATE TABLE `proycab` (
  `cp_Id` int(11) NOT NULL,
  `pry_Id` int(11) NOT NULL,
  `cp_Estado` int(1) NOT NULL COMMENT '"0" activo "1" inactivo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE `proyecto` (
  `pry_Id` int(11) NOT NULL,
  `pry_Nombre` varchar(52) NOT NULL,
  `pry_Descripcion` varchar(70) NOT NULL,
  `pry_Estado` int(1) NOT NULL DEFAULT '0',
  `us_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `us_Id` int(11) NOT NULL,
  `us_Nombre` varchar(52) NOT NULL,
  `us_Correo` varchar(52) NOT NULL,
  `us_Contraseña` varchar(70) NOT NULL,
  `us_Cuenta` varchar(52) NOT NULL,
  `us_Estado` int(1) NOT NULL COMMENT '"0" activo "1" inactivo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `criterio`
--
ALTER TABLE `criterio`
  ADD PRIMARY KEY (`cr_Id`),
  ADD KEY `cr_Idh` (`cr_Idh`),
  ADD KEY `est_Id_idx` (`est_Id`);

--
-- Indices de la tabla `detcab`
--
ALTER TABLE `detcab`
  ADD PRIMARY KEY (`dc_Id`),
  ADD KEY `cp_Id` (`cp_Id`),
  ADD KEY `est_Id` (`est_Id`);

--
-- Indices de la tabla `estandar`
--
ALTER TABLE `estandar`
  ADD PRIMARY KEY (`est_Id`);

--
-- Indices de la tabla `evidencia`
--
ALTER TABLE `evidencia`
  ADD PRIMARY KEY (`evi_Id`),
  ADD KEY `cr_Id` (`cr_Id`);

--
-- Indices de la tabla `proycab`
--
ALTER TABLE `proycab`
  ADD PRIMARY KEY (`cp_Id`),
  ADD KEY `pry_Id` (`pry_Id`);

--
-- Indices de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD PRIMARY KEY (`pry_Id`),
  ADD KEY `us_Id` (`us_Id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`us_Id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `criterio`
--
ALTER TABLE `criterio`
  MODIFY `cr_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `detcab`
--
ALTER TABLE `detcab`
  MODIFY `dc_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `estandar`
--
ALTER TABLE `estandar`
  MODIFY `est_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `evidencia`
--
ALTER TABLE `evidencia`
  MODIFY `evi_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `proycab`
--
ALTER TABLE `proycab`
  MODIFY `cp_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  MODIFY `pry_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `us_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `criterio`
--
ALTER TABLE `criterio`
  ADD CONSTRAINT `cr_Idh` FOREIGN KEY (`cr_Idh`) REFERENCES `criterio` (`cr_Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `est_Id` FOREIGN KEY (`est_Id`) REFERENCES `estandar` (`est_Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `detcab`
--
ALTER TABLE `detcab`
  ADD CONSTRAINT `detcab_ibfk_1` FOREIGN KEY (`cp_Id`) REFERENCES `proycab` (`cp_Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detcab_ibfk_2` FOREIGN KEY (`est_Id`) REFERENCES `estandar` (`est_Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `evidencia`
--
ALTER TABLE `evidencia`
  ADD CONSTRAINT `evidencia_ibfk_1` FOREIGN KEY (`cr_Id`) REFERENCES `criterio` (`cr_Id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proycab`
--
ALTER TABLE `proycab`
  ADD CONSTRAINT `pry_Id` FOREIGN KEY (`pry_Id`) REFERENCES `proyecto` (`pry_Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD CONSTRAINT `proyecto_ibfk_1` FOREIGN KEY (`us_Id`) REFERENCES `usuario` (`us_Id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;
