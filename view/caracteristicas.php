<?php

require_once("../conexion/conexion.php");
$c = new Conexion();
$conn = $c->getConexion();

$idCriterio = 1;

// Este es: $_GET['idTarea']
// $idCriterio = $_GET['idTarea']

$consulta =  "SELECT * FROM `evidencia` WHERE `cr_Id` =".$idCriterio;
$result =$conn->query($consulta);

$outpe = array();
$outpe = $result->fetch_all(MYSQLI_ASSOC);

$prueba="";
$idEvidencia = "";
$evidenciaNombre = "";




?>

<!DOCTYPE html>
<html lang="en">

<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Appraisal UNSA </a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 1</strong>
                                        <span class="pull-right text-muted">40% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 2</strong>
                                        <span class="pull-right text-muted">20% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                            <span class="sr-only">20% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 3</strong>
                                        <span class="pull-right text-muted">60% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 4</strong>
                                        <span class="pull-right text-muted">80% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Tasks</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        
                        
                        
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Evidencias<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="../pages/registrarEvidencia.php">Registrar Evidencias</a>
                                </li>
                                <li>
                                    <a href="buttons.html">Buttons</a>
                                </li>
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
    

<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Lista de  Evidencias</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            

            <div class="row">


 <div class="col-lg-12">
                   <div class="panel panel-default">
                        <div class="panel-heading">
                            Evidencias
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nombre Metrica</th>
                                            <th>Web Service</th>
                                            <th>Documento</th>
                                            <th>Option</th>
                                            <th>Option</th>
                                                                                
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                            // while ($prueba = $result->fetch_all(MYSQLI_ASSOC)) {

                                            
                                            $obj = $outpe;

                                            for ($i = 0; $i < count($obj); $i++) {
                                                $id = $obj[$i]['evi_Id'];
                                                $nombre = $obj[$i]['evi_Nombre'];
                                                $ws = $obj[$i]['evi_Archivo'];
                                                $archivo = $obj[$i]['evi_urlws'];
                                                
                                                if (strcmp($obj[$i]['evi_Estado'],"2") == 0) {
                                                    echo '
                                                    <tr class="success">.
                                                        <td>'.$id.'</td>
                                                        <td>'.$nombre .'</td>
                                                        <td>'.$ws .'</td>
                                                        <td>'.$archivo .'</td>

                                                        <td><button type="button" class="btn btn-primary" onclick= "mostrar('.$id.');">Editar</button></td>
                                                        <td><button type="button" class="btn btn-danger" onclick= "update('.$id.');">Eliminar</button></td>
                                                        
                                                        </tr>';
                                                }
                                                else if (strcmp($obj[$i]['evi_Estado'],"1") == 0) {
                                                    echo '
                                                    <tr class="warning">.
                                                        <td>'.$id.'</td>
                                                        <td>'.$nombre .'</td>
                                                        <td>'.$ws .'</td>
                                                        <td>'.$archivo .'</td>
                                                        <td><button type="button" class="btn btn-primary" onclick= "mostrar('.$id.');">Editar</button></td>
                                                        <td><button type="button" class="btn btn-danger" onclick= "update('.$id.');">Eliminar</button></td>
                                                    </tr>';

                                                }
                                                else {  
                                                    echo '
                                                    <tr class="danger">.
                                                        <td>'.$id.'</td>
                                                        <td>'.$nombre .'</td>
                                                        <td>'.$ws .'</td>
                                                        <td>'.$archivo .'</td>
                                                        <td><button type="button" class="btn btn-primary" onclick= "mostrar('.$id.');">Editar</button></td>
                                                        <td><button type="button" class="btn btn-danger" onclick= "update('.$id.');">Eliminar</button></td>
                                                        
                                                        </tr>';

                                                }
                                            


                                            }
                                            
                                        ?>

                                        <!--
                                        <tr class="success">
                                            <td>1</td>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                        </tr>
                                        <tr class="info">
                                            <td>2</td>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                        </tr>
                                        <tr class="warning">
                                            <td>3</td>
                                            <td>Larry</td>
                                            <td>the Bird</td>
                                            <td>@twitter</td>
                                        </tr>
                                        <tr class="danger">
                                            <td>4</td>
                                            <td>John</td>
                                            <td>Smith</td>
                                            <td>@jsmith</td>
                                        </tr>
                                        -->
                                    </tbody>
                                </table>
                                <hr>
                                <div>
                                    <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modalForm">
    Agregar Evidencia
</button>
                                </div>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
            </div>
        </div>

    </div>


    

    <div class="modal fade" id="modal_exito" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Esta seguro que desea eliminar esta evidencia.</h3>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" onclick="mostrarMensaje(5)"  data-dismiss="modal">Eliminar</button>
                    <button type="button" class="btn btn-default"  data-dismiss="modal">Cancelar</button>
                </div>
            </div>

        </div>
    </div>
    <!--
    MODAL FOR ADD

-->

<div class="modal  fade" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel" style="text-align:center;font-weight: bold; ">Registrar Evidencia</h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form role="form" method="POST" enctype="multipart/form-data" action="../controller/evidencia/saveEvidencia.php">

                                        <div class="form-group">
                                            <label>Nombre</label>
                                            <input class="form-control" name="evidencia_nombre" id="evidencia_nombre">
                                            <p class="help-block">Ingrese un nombre para la evidencia.</p>
                                        </div>
                                        <div class="form-group">
                                            <label>Descripción</label>
                                            <textarea class="form-control" rows="3" id="evidencia_descripcion" name="evidencia_descripcion"></textarea>
                                        </div>



                                        <div class="form-group">
                                            <label>Estado</label>
                                            <select class="form-control" id="evidencia_estado" name="evidencia_estado">
                                                <option value="2">Bueno</option>
                                                <option value="1">Regular</option> 
                                                <option value="0">Malo</option>     
                                            </select>
                                        </div>
                                        
                                        
                                        <div class="form-group">
                                            <label>Archivo </label>
                                            <input id="evidencia_file" name="evidencia_file" type="file">
                                        </div>

                                        <div class="form-group">
                                            <label>URL</label>
                                            <textarea id="evidencia_url" name="evidencia_url" class="form-control" rows="3"></textarea>
                                        </div>
                                        
                                         
                                    </form>
               
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
               <?php echo '<button type="button" class="btn btn-primary submitBtn" onclick="addEvidencia('.$idCriterio.')">Agregar</button>' ?>
            </div>
        </div>
    </div>
</div>

<!--
    EDIT MODAL
-->
<div class="modal  fade" id="modalEditForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel" style="text-align:center;font-weight: bold; ">Editar Evidencia</h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form role="form" method="POST" enctype="multipart/form-data" action="../controller/evidencia/saveEvidencia.php">

                                        
                                        <input type="hidden" id="e_idEvi" name="e_idEvi">
                                        <div class="form-group">
                                            <label>Nombre</label>
                                            <input class="form-control" name="e_evidencia_nombre" id="e_evidencia_nombre">
                                            <p class="help-block">Ingrese un nombre para la evidencia.</p>
                                        </div>
                                        <div class="form-group">
                                            <label>Descripción</label>
                                            <textarea class="form-control" rows="3" id="e_evidencia_descripcion" name="e_evidencia_descripcion"></textarea>
                                        </div>



                                        <div class="form-group">
                                            <label>Estado</label>
                                            <select class="form-control" id="e_evidencia_estado" name="e_evidencia_estado">
                                                <option value="2">Bueno</option>
                                                <option value="1">Regular</option> 
                                                <option value="0">Malo</option>     
                                            </select>
                                        </div>
                                        
                                        
                                        <div class="form-group">
                                            <label>Archivo </label>
                                            <input id="e_evidencia_file" name="e_evidencia_file" type="file">
                                        </div>

                                        <div class="form-group">
                                            <label>URL</label>
                                            <textarea id="e_evidencia_url" name="e_evidencia_url" class="form-control" rows="3"></textarea>
                                        </div>
                                        
                                         
                                    </form>
               
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
               <?php echo '<button type="button" class="btn btn-primary submitBtn" onclick="editEvidencia()">Agregar</button>' ?>
            </div>
        </div>
    </div>
</div>


</body>
<script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>


<script type="text/javascript">
    var ayuda = 1000;
    var hola1 = '';
                                            
    function mostrar(id){
     
        //window.location.replace("../pages/editarEvidencia.php?id="+id);
               

        
        $.ajax({
        url: "../controller/evidencia/getEvidenciaById.php",
        type: 'POST',
        data: {"evidencia_Id":id},      
        success: function(result){

            if(result){
                var objs = $.parseJSON(result);
                
                                
                $('#e_evidencia_nombre').val(objs[0]['evi_Nombre']);
                $('#e_evidencia_descripcion').val(objs[0]['evi_Descripcion']);
                $("#e_evidencia_estado option[value="+objs[0]['evi_Estado']+"]").attr('selected', 'selected');
                $('#e_evidencia_url').val(objs[0]['evi_urlws']);
                $('#e_evidencia_file').attr('value',objs[0]['evi_Archivo']);            
                $('#e_idEvi').val(id);          
                //window.location.replace("../index");
                $('#modalEditForm').modal('show');
            }
            else{
                alert('ocurrio algun ERROR, vuelva a intentarlo ');
            }   
        },
        error: function(){
            alert('Ocurrio un erro en el Proceso');
        }
    });
        /*$.ajax({

            type: 'POST',
            url: '../controller/evidencia/getEvidenciaById.php',
            data: {
                "evidencia_Id": id,
            },
            success: function (data) {
                alert(data);
                window.location.replace("../pages/editarEvidencia.php?");
                alert("se envió exitosamente codigo a su correo!");
            }
        });  */
    }
    function update(id){
    //rellenar los grupos 
        $.ajax({
            type: 'GET',
            url: '../controller/evidencia/evidenciaEliminar.php',
            data: {
                idMetrica: id,
            },
            success: function (data) {
                console.log("codigo="+data);
                location.reload();
                alert("se elimino correctamente!");
            }
        });                        
    }
</script>
<script>
$("#modalForm").on('shown.bs.modal', function(){
    $.ajax({
        url: "../controller/evidencia/getEvaluacion.php",
        type: 'POST',
        data: {"data":"fsdf"},      
        success: function(result){
            if(result){
                var i=0;
               var objs = $.parseJSON(result); 
              
               for(var i=0; i<objs.length;i++){

               $('#FK_evaluacion_id').append($("<option></option>").attr("value",objs[i]['eva_Id']).text(objs[i]['eva_Nombre'])); 
               }
                
            }
            else{
                alert('ocurrio algun ERROR, vuelva a intentarlo ');
            }   
        },
        error: function(){
            alert('Ocurrio un erro en el Proceso');
        }
    });
});

function editEvidencia(){
    var file_data = $('#e_evidencia_file').prop('files')[0];   
    var form_data = new FormData();  
                   
    form_data.append('evidencia_nombre', $("#e_evidencia_nombre").val());
    form_data.append('evidencia_descripcion', $("#e_evidencia_descripcion").val());
    form_data.append('evidencia_estado', $("#e_evidencia_estado").val());
    form_data.append('evidencia_url', $("#e_evidencia_url").val());   
    form_data.append('evidencia_file', file_data);
    form_data.append('id', $("#e_idEvi").val());

    $.ajax({
        url: '../controller/evidencia/updateEvidencia.php',       
        type: 'POST',  
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        data: form_data,      
        success: function(result){
            if(result){
            alert("cambiado correctamente!");              
                window.location.replace("../view/caracteristicas.php");
            }
            else{
                alert('ocurrio algun ERROR, vuelva a intentarlo ');
            }   

        },
        error: function(){
            alert('Ocurrio un erro en el Proceso');
        }
    });

}

function addEvidencia(id){
    
    var idCriterio = id;
    var file_data = $('#evidencia_file').prop('files')[0];   
    var form_data = new FormData();  
                   
    form_data.append('evidencia_nombre', $("#evidencia_nombre").val());
    form_data.append('evidencia_descripcion', $("#evidencia_descripcion").val());
    form_data.append('evidencia_estado', $("#evidencia_estado").val());
    form_data.append('evidencia_url', $("#evidencia_url").val());
    form_data.append('FK_evaluacion_id', idCriterio);
    form_data.append('evidencia_file', file_data);
    $.ajax({
        url: '../controller/evidencia/saveEvidencia.php',       
        type: 'POST',  
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        data: form_data,      
        success: function(result){
            if(result){
            alert("agregado correctamente!");              
                window.location.replace("../view/caracteristicas.php");
            }
            else{
                alert('ocurrio algun ERROR, vuelva a intentarlo ');
            }   

        },
        error: function(){
            alert('Ocurrio un erro en el Proceso');
        }
    });
}
</script>
</html>


